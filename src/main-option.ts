import { createApp,provide,h } from 'vue'
import App from './App.vue'
import { ApolloClient,createHttpLink,InMemoryCache } from '@apollo/client/core'
import { createApolloProvider } from '@vue/apollo-option'

//Para conectarnos a la API del servicio. Es el endpoint
const httpLink=createHttpLink({
    uri:'http://localhost:4000/graphql'
})

//implementar la caché
const cache=new InMemoryCache()

//creamos el cliente Apollo
const apolloClient=new ApolloClient({
    link:httpLink,
    cache:cache
})

//creamos proveedor
const apolloProvider=createApolloProvider({
    defaultClient:apolloClient
})

//lo proveemos al sistema
createApp({
    render:()=>h(App)
}).use(apolloProvider).mount('#app')