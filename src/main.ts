import { createApp,provide,h } from 'vue'
import App from './App.vue'
import { ApolloClient,createHttpLink,InMemoryCache } from '@apollo/client/core'
import { DefaultApolloClient } from '@vue/apollo-composable'

//Para conectarnos a la API del servicio. Es el endpoint
const httpLink=createHttpLink({
    uri:'http://localhost:4000/graphql'
})

//implementar la caché
const cache=new InMemoryCache()

//creamos el cliente Apollo
const apolloClient=new ApolloClient({
    link:httpLink,
    cache:cache
})

//crea una instancia de una aplicación Vue
createApp({
    //configuración que se ejecuta antes de la creación de la aplicación
    setup(){
        // proporcionar el cliente de Apollo
        // cualquier componente hijo de este componente principal podrá acceder al cliente de Apollo 
        provide(DefaultApolloClient,apolloClient)
    },
    //especifica el componente raíz que será montado en el DOM.
    //se está utilizando h(App) para renderizar el componente App.
    //h es una abreviatura de createElement y es una función de bajo nivel que Vue utiliza para renderizar componentes.
    render:()=>h(App)
}).mount('#app')
